/*
 * ======================================================================================================================== *
 * This project was created in order to receive data from the ERLS RX900 device connected to the STM32F103C8 'Blue pill',
 * and further manipulate the data.
 *
 *  @file main.c
 *  @data create: 24.11.2023
 *  @author: Chernishevskii Leonid Romanovich
 * ======================================================================================================================== *
 */
#include "main.h"

struct Usart2 {
	uint8_t tx_buffer[64]; //Буфер под выходящие данные
	uint8_t rx_buffer[64]; //Буфер под входящие данные
	uint16_t rx_counter; //Счетчик приходящих данных типа uint8_t по USART
	uint16_t rx_len; //Количество принятых байт после сработки флага IDLE
};

struct Usart2 usart2;

int main(void) {

	CMSIS_Debug_init();                 //Настройка Debug (Serial Wire)
	CMSIS_RCC_SystemClock_72MHz(); //Настрока тактирования микроконтроллера на частоту 72MHz
	CMSIS_SysTick_Timer_init();         //Инициализация системного таймера
	CMSIS_PC13_OUTPUT_Push_Pull_init(); //Пример настройки ножки PC13 в режим Push-Pull 50 MHz

	CMSIS_USART2_Init();                //Инициализация USART2

//	uint32_t ms = 500;  // Виставляем время задержки

	while (1) {

		if (READ_BIT(USART2->SR, USART_SR_RXNE)) {
			//Если пришли данные по USART
			usart2.rx_buffer[usart2.rx_counter] = USART2->DR; //Считаем данные в соответствующую ячейку в rx_buffer
			usart2.rx_counter++; //Увеличим счетчик принятых байт на 1
		}

		if (READ_BIT(USART2->SR, USART_SR_IDLE)) {
			//Если прилетел флаг IDLE
			USART2->DR; //Сбросим флаг IDLE
			usart2.rx_len = usart2.rx_counter; //Узнаем, сколько байт получили
			usart2.rx_counter = 0; //сбросим счетчик приходящих данных
		}

//		CMSIS_Blink_PC13(ms);

		printf("START \r\n");
		printf("Buffer - %p \r\n", usart2.rx_buffer);
		printf("Len - %i \r\n", usart2.rx_len);
		printf("\r\n");


		/*   ERROR UART */
		if (READ_BIT(USART2->SR, USART_SR_FE)) {
		    // Обработка ошибки кадрирования (Framing Error)
			printf("Framing Error");
		}

		if (READ_BIT(USART2->SR, USART_SR_PE)) {
		    // Обработка ошибки четности (Parity Error)
			printf("Parity Error");
		}
	}
}
